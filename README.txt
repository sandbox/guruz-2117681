Amazon Affiliate Link Globalizer
--------------------------------

Rewrites Amazon.com/Amzn.com and forwards the visitor to 'their' country specific Amazon store (using IP Geolocation by A-FWD.com). 

http://a-fwd.com/


INSTALLATION
------------

Install and activate the module as usual, see http://drupal.org/node/70151 for more information.

Enable the filter for every Text format you need, for example "Filtered HTML":
	Configuration > Text formats > Filtered HTML > configure
		Set checkbox in the section 'Enabled filters'
		Move filter to the end of the filter list.


CONFIGURATION
-------------

You can optionally enter your Amazon Affiliate/Tracking IDs for every needed country.

Click the 'configure' button in the modules section or follow the link at:
  Administer > Configuration > Content 


CONTACT
-------

Author: Markus Goetz (Woboq),
		Mathias Vogler

http://www.woboq.com/

Licensed under GNU General Public License v2

